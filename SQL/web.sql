CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

create TABLE USER(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date date not null,
password varchar(255) not null,
create_date datetime not null,
update_date datetime not null,
);

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUE('admin','管理者',1998-01-31,'password',NOW(),NOW());
