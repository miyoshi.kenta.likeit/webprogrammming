<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>ユーザ情報詳細照会</title>
</head>

<body>
    <div class="header">
        <header>

            <div Style="float: left; margin-left:1200px;" class="row col-4" align="right">
                <p>
                    ユーザ名　さん
                </p>
                <p>
                    <!--hrefはボタンのタグ-->
                    <a href="LogoutServlet">ログアウト</a>
                </p>
            </div>
        </header>
    </div>
    <h1>
        <div style="padding-top :20px;">

            <center>ユーザ情報詳細照会</center>
        </div>
        </div>
        　
    </h1>
    <div class="col-sm-12" align="center">
        <table border="0">

            <tr height="50">
                <th>
                    ログインID
                </th>
                <td align="right">
                   ${user.loginId}
                </td>
            </tr>
            <tr height="50px">
                <th>
                    ユーザ名
                </th>
                　<td align="right">${user.name}
                </td>
            </tr>
            <tr height="50">
                <th>
                    生年月日
                </th>
                <td align="right">${user.birthDate}
                </td>
            </tr>
            <tr height="50">
                <th>
                    登録日時
                </th>
                <td align="right">
                    ${user.createDate}
                </td>
            </tr>
            <tr height="50">
                <th>
                    更新日時
                </th>
                <td align="right">
                     ${user.updateDate}
                </td>
            </tr>

        </table>
    </div>

    <p>
        <a href="UserAllServlet">
            戻る
        </a>
    </p>





</body></html>