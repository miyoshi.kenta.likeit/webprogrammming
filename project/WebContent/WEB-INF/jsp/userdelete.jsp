<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>ユーザ一覧</title>
  </head>
  <body>
       <div class="header">
        <header>

        <div Style="float: left; margin-left:1200px;" class="row col-4" align="right">
          <p>
            ユーザ名　さん
          </p>
          <p>
           <!--hrefはボタンのタグ-->
            <a href="LogoutServlet">ログアウト</a>
          </p>
        </div>
       </header>
      </div>
    　<h1>
          <div style="padding-top :20px;">
              <center>ユーザ削除確認</center>
          </div>
      </h1>
      <div class="col-sm-12" align="center">
        <p>
            ログインID:${user.loginId}<br>
            を本当に削除してもよろしいでしょうか。
        </p>
      </div>

      <div class="col-sm-12" align="center">
        <table border="0">
            <tr height="50">
                <form action="UserAllServlet" method="get">
                <input type="submit" value="キャンセル">
                </form>

                <form action="UserDeleteServlet" method="post">
                <input name="id" value="${user.id}" type="hidden">
                <input type="submit" value="OK">
                </form>
            </tr>
          </table>
    　</div>