<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />


    <title>ユーザ一覧</title>
</head>

<body>
    <div class="header">
        <header>

            <div Style="float: left; margin-left:1200px;" class="row col-4" align="right">

                <p>
                ${userInfo.name} さん
                </p>
                <p>
                    <!--hrefはボタンのタグ-->
                    <a href="LogoutServlet">ログアウト</a>
                </p>
            </div>
        </header>
    </div>
    　<h1>
        <div style="padding-top :20px;">
            <center>ユーザ一覧</center>
        </div>
        　
    </h1>

    	<p align="right" class="mr-5">
      	<a href="UserRegistration">新規登録</a>
     	</p>

    <form action="UserAllServlet" method="post">

        <div class="col-sm-12" align="center">
            <table border="0">
                <tr height="50">
                    <th>
                        ログインID
                    </th>
                    <td>
                        <input type="text" name="user_id">
                    </td>
                </tr>
                <tr>
                    <th>
                        ユーザ名
                    </th>
                    　<td>
                        <input type="text" name="name">
                    </td>
                </tr>
                <tr>
                    <th>
                        生年月日
                    </th>
                    <td>
                        <input type="date" name="birthdate1" >
                    </td>
                    <td align="center">
                        ~
                    </td>
                    <td>
                        <input type="date" name="birthdate2" size="10">
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-sm-12" align="center">
            <input type="submit" value="検索" size="24">
        </div>

    </form>
    <hr>

    <div class="col-sm-12" align="center">
        <table border="1">

            <thaed>
                <tr>
                    <th>ログインID</th>
                    <th>ユーザ名</th>
                    <th>生年月日</th>
                    <th></th>
                </tr>
            </thaed>
            <tbody>
             <c:forEach var="user" items="${userList}" >
                <tr>
                   	<td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                    <td>
                        <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                         <c:if test="${userInfo.loginId == user.loginId || userInfo.loginId == 'admin'}" >
                      <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>

                       <c:if test="${userInfo.loginId == 'admin'}" >
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                        </c:if>
                    </td>
                </tr>
              </c:forEach>
            </tbody>

        </table>
    </div>


</body></html>