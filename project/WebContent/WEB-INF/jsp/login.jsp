<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
     <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>ログイン</title>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

     </head>
     <body>
        <h1>
          <div style="padding:20px;">
            <center>ログイン画面</center>
          </div>
        </h1>

　     <c:if test="${errMsg != null}" >
	     <div class="alert alert-danger" role="alert">
		   ${errMsg}
		 </div>
	   </c:if>


         <form action="LoginServlet" method="post">

          <div class="col-sm-12" align="center">
          <table border="0">
            <tr height="50">
             <th>
              ログインID
             </th>
              <td align="right">
                <input type="text" name="user_id" >
              </td>
            </tr>
            <tr>
             <th>
                パスワード
             </th>
             　<td>
               　<input type="password" name="password">
              </td>
            </tr>
           </table>
           </div>

            <div class="col-sm-12" align="center">
             <input type="submit" value="ログイン">
            </div>

          </form>


      </body>
  </html>