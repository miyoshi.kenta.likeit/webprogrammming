<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <link href="style.css" rel="stylesheet" type="text/css" />


    <title>Hello, world!</title>
  </head>
  <body>
      <div class="header">
        <header>

        <div Style="float: left; margin-left:1200px;" class="row col-4">
          <p>
           ${userInfo.name}
          </p>
          <p>
           <!--hrefはボタンのタグ-->
            <a href="LogoutServlet">ログアウト</a>
          </p>
        </div>
       </header>
      </div>

        <h1>
          <div style="padding:20px;">
            <center>ユーザ新規登録</center>
          </div>
        </h1>

        <c:if test="${errerMsg != null}" >
        <div class="alert alert-danger" role="alert">
		   ${errerMsg}
		 </div>
	   </c:if>
    <form action="UserRegistration" method="post">

          <div class="col-sm-12" align="center">
          <table border="0">
            <tr height="50">
             <th>
              ログインID
             </th>
              <td align="right">
                <input type="text" name="user_id" >
              </td>
            </tr>
            <tr height="50px">
             <th>
                パスワード
             </th>
             　<td>
               　<input type="password" name="password">
              </td>
            </tr>
              <tr height="50">
             <th>
              パスワード(確認)
             </th>
              <td align="right">
                <input type="password" name="password_check">
              </td>
            </tr>
              <tr height="50">
             <th>
            　ユーザ名
             </th>
              <td align="right">
                <input type="text" name="user_name" >
              </td>
            </tr>
              <tr height="50">
             <th>
              生年月日
             </th>
              <td align="right">
                <input type="text" name="user_birth" >
              </td>
            </tr>
           </table>
           </div>

            <div class="col-sm-12" align="center">
             <input type="submit" value="登録">
            </div>

          </form>
      <p>
        <a href="UserAllServlet">戻る</a>
      </p>
  </body>
</html>
