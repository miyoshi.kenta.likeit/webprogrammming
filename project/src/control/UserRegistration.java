package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegistration
 */
@WebServlet("/UserRegistration")
public class UserRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistration() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログイン画面に遷移させる。

		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usernew.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定(String loginId, String name, Date birthDate, String password)
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("user_id");
		String password = request.getParameter("password");
		String password_check = request.getParameter("password_check");
		String user_name = request.getParameter("user_name");
		String user_birth = request.getParameter("user_birth");

		if (!(password.equals(password_check))) {
			request.setAttribute("errerMsg", "入力された内容は正しくありません");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usernew.jsp");
			dispatcher.forward(request, response);
			return;

		}
		if (loginId.equals("") || password.equals("") || password_check.equals("") || user_name.equals("") || user_birth.equals("")) {

			request.setAttribute("errerMsg", "入力された内容は正しくありません");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usernew.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//ログインID確認用
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginID(loginId);
		if (!(user == null)) {
			request.setAttribute("errerMsg", "入力された内容は正しくありません");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usernew.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//新規登録用

		userDao.newRegistration(loginId, user_name, user_birth, password);

		response.sendRedirect("UserAllServlet");
	}

}
