package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpsateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログイン画面に遷移させる！！
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user = userDao.usersInformation(id);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user", user);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String passwordcheck = request.getParameter("password_check");
		String password = request.getParameter("password");
		String username = request.getParameter("user_name");
		String userbirth = request.getParameter("user_birth");

		if (!(password.equals(passwordcheck))) {

			request.setAttribute("errerMsg", "入力された内容は正しくありません");
			UserDao userDao = new UserDao();
			User user = userDao.usersInformation(id);
			request.setAttribute("user", user);

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;

		}
		if (username.equals("") || userbirth.equals("")) {

			request.setAttribute("errerMsg", "入力された内容は正しくありません");
			UserDao userDao = new UserDao();
			User user = userDao.usersInformation(id);
			request.setAttribute("user", user);
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if (password.equals("") && passwordcheck.equals("")) {
			UserDao userDao = new UserDao();
			userDao.userUpdatecheck(username, userbirth, id);
			response.sendRedirect("UserAllServlet");
			return;

		}

		UserDao userDao = new UserDao();
		userDao.userUpdate(password, username, userbirth, id);

		response.sendRedirect("UserAllServlet");

	}

}
